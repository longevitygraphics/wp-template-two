function sticky_header_initialize(){
	var site_header = jQuery('#site-header');
	var site_content = jQuery('#site-content');

	if(site_header[0] && site_header.hasClass('sticky-header')){
		var headerHeight = site_header.outerHeight();
		site_content.css('paddingTop', headerHeight);
		site_header.fadeIn();
	}
}
